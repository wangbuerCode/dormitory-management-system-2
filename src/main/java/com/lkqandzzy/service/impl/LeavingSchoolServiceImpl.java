package com.lkqandzzy.service.impl;

import com.lkqandzzy.dao.LeavingSchoolDao;
import com.lkqandzzy.entity.LeavingSchool;
import com.lkqandzzy.entity.PageInfo;
import com.lkqandzzy.service.LeavingSchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 邹靓仔and小帅龙
 * @date 2019/12/29-13:51-星期日-DormitoryManager-ssm
 */
@Service("LeavingSchoolService")
@Transactional
public class LeavingSchoolServiceImpl implements LeavingSchoolService {

    // classDao
    @Autowired
    private LeavingSchoolDao leavingSchoolDao;


    //分页查询
    @Override
    public PageInfo<LeavingSchool> findPageInfo(Integer k_classid, String k_dormbuilding, Integer pageIndex, Integer pageSize) {
        PageInfo<LeavingSchool> pi = new PageInfo<LeavingSchool>();
        pi.setPageIndex(pageIndex);
        pi.setPageSize(pageSize);
        //获取总条数
        Integer totalCount = leavingSchoolDao.totalCount(k_classid,k_dormbuilding);
        if (totalCount>0){
            pi.setTotalCount(totalCount);
            //每一页显示班级信息数
            //currentPage = (pageIndex-1)*pageSize  当前页码数减1*最大条数=开始行数
            List<LeavingSchool> leavingSchoolList =	leavingSchoolDao.getLeavingSchoolList(k_classid,k_dormbuilding,
                    (pi.getPageIndex()-1)*pi.getPageSize(),pi.getPageSize());
            pi.setList(leavingSchoolList);
        }
        return pi;
    }

    @Override
    public List<LeavingSchool> getAll(){
        List<LeavingSchool> leavingSchoolList = leavingSchoolDao.getAll();
        return  leavingSchoolList;
    }

    //通过id删除班级信息
    @Override
    public int deleteLeavingSchool(Integer k_id) {
        return leavingSchoolDao.deleteLeavingSchool(k_id);
    }

    //添加班级信息
    @Override
    public int addLeavingSchool(LeavingSchool leavingSchool) {
        return leavingSchoolDao.addLeavingSchool(leavingSchool);
    }


    //修改班级信息
    @Override
    public int updateLeavingSchool(LeavingSchool leavingSchool) {
        return leavingSchoolDao.updateLeavingSchool(leavingSchool);
    }
    @Override
    public LeavingSchool findLeavingSchoolById(Integer k_id){
        LeavingSchool c = leavingSchoolDao.findLeavingSchoolById(k_id);
        return  c;
    }

}
