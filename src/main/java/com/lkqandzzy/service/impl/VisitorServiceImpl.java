package com.lkqandzzy.service.impl;

import com.lkqandzzy.dao.VisitorDao;
import com.lkqandzzy.entity.PageInfo;
import com.lkqandzzy.entity.Visitor;
import com.lkqandzzy.service.VisitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 邹靓仔and小帅龙
 * @date 2019/12/29-13:54-星期日-DormitoryManager-ssm
 */
@Service("visitorService")
@Transactional
public class VisitorServiceImpl implements VisitorService {
    // 注入studentDao
    @Autowired
    private VisitorDao visitorDao;


    //分页查询
    @Override
    public PageInfo<Visitor> findPageInfo(String v_name, Integer v_phone , Integer pageIndex, Integer pageSize) {
        PageInfo<Visitor> pi = new PageInfo<Visitor>();
        pi.setPageIndex(pageIndex);
        pi.setPageSize(10);
        //获取总条数
        Integer totalCount = visitorDao.totalCount(v_name,v_phone);
        if (totalCount>0){
            pi.setTotalCount(totalCount);
            //每一页显示学生信息数
            //currentPage = (pageIndex-1)*pageSize  当前页码数减1*最大条数=开始行数
            List<Visitor> visitorList =	visitorDao.getVisitorList(v_name,v_phone,
                    (pi.getPageIndex()-1)*pi.getPageSize(),pi.getPageSize());
            pi.setList(visitorList);
        }
        return pi;
    }

    @Override
    public List<Visitor> getAll(){
        List<Visitor> visitorList = visitorDao.getAll();
        return visitorList;
    }

    //添加学生信息
    @Override
    public  int addVisitor(Visitor visitor) {
        return visitorDao.addVisitor(visitor);
    }


}
