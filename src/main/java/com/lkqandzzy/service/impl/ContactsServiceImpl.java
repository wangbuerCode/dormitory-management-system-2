package com.lkqandzzy.service.impl;

import com.lkqandzzy.dao.ContactsDao;
import com.lkqandzzy.entity.Contacts;
import com.lkqandzzy.entity.PageInfo;
import com.lkqandzzy.service.ContactsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("ContactsService")
@Transactional
public class ContactsServiceImpl implements ContactsService {

    // classDao
    @Autowired
    private ContactsDao contactsDao;


    //分页查询
    @Override
    public PageInfo<Contacts> findPageInfo(String c_subordinate,Integer pageIndex, Integer pageSize) {
        PageInfo<Contacts> ci = new PageInfo<Contacts>();
        ci.setPageIndex(pageIndex);
        ci.setPageSize(10);
        //获取总条数
        Integer totalCount = contactsDao.totalCount(c_subordinate);
        if (totalCount>0){
            ci.setTotalCount(totalCount);
            //每一页显示联系人信息数
            //currentPage = (pageIndex-1)*pageSize  当前页码数减1*最大条数=开始行数
            List<Contacts> contactsList =	contactsDao.getDormitoryList(c_subordinate, (ci.getPageIndex()-1)*ci.getPageSize(),ci.getPageSize());
            ci.setList(contactsList);
        }
        return ci;
    }

    @Override
    public List<Contacts> getAll(){
        List<Contacts> contactsList = contactsDao.getAll();
        return contactsList;
    }

    //添加联系人信息
    @Override
    public int addContacts(Contacts contacts) {
        return contactsDao.addContacts(contacts);
    }

    //通过id删除联系人信息
    @Override
    public int deleteContacts(Integer c_id) {
        return contactsDao.deleteContacts(c_id);
    }

    //修改联系人信息
    @Override
    public int updateContacts(Contacts contacts) {
        return contactsDao.updateContacts(contacts);
    }

    @Override
    public Contacts findContactsById (Integer c_id){
        Contacts c = contactsDao.findContactsById(c_id);
        return  c;
    }

}
