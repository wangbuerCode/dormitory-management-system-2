package com.lkqandzzy.service.impl;

import com.lkqandzzy.dao.DlreturnDao;
import com.lkqandzzy.entity.Dlreturn;
import com.lkqandzzy.entity.PageInfo;
import com.lkqandzzy.service.DormCleanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 邹靓仔and小帅龙
 * @date 2019/12/29-13:44-星期日-DormitoryManager-ssm
 */
@Service("dlreturnService")
@Transactional
public class DlreturnServiceImpl implements DormCleanService.DlreturnService {

    // 注入studentDao
    @Autowired
    private DlreturnDao dlreturnDao;

    @Override
    public PageInfo<Dlreturn> findPageInfo(String l_lreturnnanme, Integer l_studentid, Integer l_dormitoryid, String reason, Integer pageIndex, Integer pageSize) {

        PageInfo<Dlreturn> pi = new PageInfo<Dlreturn>();
        pi.setPageIndex(pageIndex);
        pi.setPageSize(5);
        //获取总条数
        Integer totalCount = dlreturnDao.totalCount(l_lreturnnanme,l_studentid,l_dormitoryid,reason);
        if (totalCount>0){
            pi.setTotalCount(totalCount);
            //每一页显示学生信息数
            //currentPage = (pageIndex-1)*pageSize  当前页码数减1*最大条数=开始行数
            List<Dlreturn> studentList =	dlreturnDao.getDlreturnList(l_lreturnnanme,l_studentid,l_dormitoryid,reason,
                    (pi.getPageIndex()-1)*pi.getPageSize(),pi.getPageSize());
            pi.setList(studentList);
        }
        return pi;
    }

    @Override
    public int deleteDlreturn(Integer l_id) {
        return dlreturnDao.deleteDlreturn(l_id);
    }

    @Override
    public int addDlreturn(Dlreturn dlreturn) {
        return dlreturnDao.addDlreturn(dlreturn);
    }

    @Override
    public int updateDlreturn(Dlreturn dlreturn) {
        return dlreturnDao.updateDlreturn(dlreturn);
    }

    @Override
    public Dlreturn findDlreturnId(Integer l_id) {
        Dlreturn s = dlreturnDao.findDlreturnId(l_id);
        return  s;
    }

    @Override
    public List<Dlreturn> getAll() {
        List<Dlreturn> studentList = dlreturnDao.getAll();
        return studentList;
    }
}
