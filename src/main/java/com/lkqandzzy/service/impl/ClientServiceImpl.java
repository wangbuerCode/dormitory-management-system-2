package com.lkqandzzy.service.impl;

import com.lkqandzzy.dao.ClientDao;
import com.lkqandzzy.entity.Client;
import com.lkqandzzy.entity.PageInfo;
import com.lkqandzzy.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("ClientService")
@Transactional
public class ClientServiceImpl implements ClientService {

    // classDao
    @Autowired
    private ClientDao clientDao;


    //分页查询
    @Override
    public PageInfo<Client> findPageInfo(String d_name,Integer pageIndex, Integer pageSize) {
        PageInfo<Client> pi = new PageInfo<Client>();
        pi.setPageIndex(pageIndex);
        pi.setPageSize(10);
        //获取总条数
        Integer totalCount = clientDao.totalCount(d_name);
        if (totalCount>0){
            pi.setTotalCount(totalCount);
            //每一页显示宿舍信息数
            //currentPage = (pageIndex-1)*pageSize  当前页码数减1*最大条数=开始行数
            List<Client> clientList =	clientDao.getDormitoryList(d_name, (pi.getPageIndex()-1)*pi.getPageSize(),pi.getPageSize());
            pi.setList(clientList);
        }
        return pi;
    }

    @Override
    public List<Client> getAll(){
        List<Client> clientList = clientDao.getAll();
        return clientList;
    }

    //添加客户信息
    @Override
    public int addClient(Client client) {
        return clientDao.addClient(client);
    }

    //通过id删除客户信息
    @Override
    public int deleteClient(Integer d_id) {
        return clientDao.deleteClient(d_id);
    }

    //修改客户信息
    @Override
    public int updateClient(Client client) {
        return clientDao.updateClient(client);
    }

    @Override
    public Client findClientById (Integer d_id){
        Client d = clientDao.findClientById(d_id);
        return  d;
    }
}
