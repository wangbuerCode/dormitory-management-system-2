package com.lkqandzzy.service;

import com.lkqandzzy.entity.PageInfo;
import com.lkqandzzy.entity.StudentClean;

import java.util.List;

/**
 * @author 邹靓仔and小帅龙
 * @date 2019/12/29-13:36-星期日-DormitoryManager-ssm
 */
public interface StudentCleanService {

    //分页查询
     PageInfo<StudentClean> findPageInfo(Integer s_studentid, String s_name, Integer s_dormitoryid, Integer pageIndex, Integer pageSize);

     int addStudentClean(StudentClean studentclean);    //添加宿舍信息
     int deleteStudentClean(Integer g_id);   //删除宿舍信息
     int updateStudentClean(StudentClean studentclean); //修改宿舍信息
     StudentClean findStudentCleanById(Integer g_id);
     List<StudentClean> getAll();
}
