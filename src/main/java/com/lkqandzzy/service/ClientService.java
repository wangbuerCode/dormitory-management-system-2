package com.lkqandzzy.service;

import com.lkqandzzy.entity.Client;
import com.lkqandzzy.entity.PageInfo;

import java.util.List;
public interface ClientService {
    //分页查询
     PageInfo<Client> findPageInfo(String d_name,Integer pageIndex, Integer pageSize);

     int addClient(Client client);    //添加客户信息
     int deleteClient(Integer d_id);   //删除客户信息
     int updateClient(Client client); //修改客户信息
     Client findClientById(Integer d_id);
     List<Client> getAll();
}
