package com.lkqandzzy.service;

import com.lkqandzzy.entity.DormRepair;
import com.lkqandzzy.entity.PageInfo;

import java.util.List;

/**
 * @author 邹靓仔and小帅龙
 * @date 2019/12/29-13:35-星期日-DormitoryManager-ssm
 */
public interface DormRepairService {
    //分页查询
     PageInfo<DormRepair> findPageInfo(Integer d_id, String d_dormbuilding, Integer pageIndex, Integer pageSize);

     int addDormRepair(DormRepair dormrepair);    //添加宿舍信息
     int deleteDormRepair(Integer r_id);   //删除宿舍信息
     int updateDormRepair(DormRepair dormrepair); //修改宿舍信息
     DormRepair findDormRepairById(Integer r_id);
     List<DormRepair> getAll();
}
