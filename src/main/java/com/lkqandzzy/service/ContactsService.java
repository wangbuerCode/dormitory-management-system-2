package com.lkqandzzy.service;

import com.lkqandzzy.entity.Contacts;
import com.lkqandzzy.entity.PageInfo;

import java.util.List;

public interface ContactsService {
    //分页查询
     PageInfo<Contacts> findPageInfo(String c_subordinate, Integer pageIndex, Integer pageSize);

     int addContacts(Contacts contacts);    //添加联系人信息
     int deleteContacts(Integer c_id);   //删除联系人信息
     int updateContacts(Contacts contacts); //修改联系人信息
    Contacts findContactsById(Integer c_id);
     List<Contacts> getAll();
}
