package com.lkqandzzy.service;

import com.lkqandzzy.entity.PageInfo;
import com.lkqandzzy.entity.Visitor;

import java.util.List;

/**
 * @author 邹靓仔and小帅龙
 * @date 2019/12/29-13:38-星期日-DormitoryManager-ssm
 */
public interface VisitorService {

    //分页查询
     PageInfo<Visitor> findPageInfo(String v_name, Integer v_phone, Integer pageIndex, Integer pageSize);
     int addVisitor(Visitor visitor);   //添加访客信息
     List<Visitor> getAll();
}
