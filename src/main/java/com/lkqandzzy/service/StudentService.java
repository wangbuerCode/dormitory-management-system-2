package com.lkqandzzy.service;

import com.lkqandzzy.entity.PageInfo;
import com.lkqandzzy.entity.Student;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 邹靓仔and小帅龙
 * @date 2019/12/29-13:37-星期日-DormitoryManager-ssm
 */
public interface StudentService {

    //分页查询
     PageInfo<Student> findPageInfo(String s_name, Integer s_studentid, Integer s_classid,
                                          String s_classname, Integer pageIndex, Integer pageSize);

     int deleteStudent(Integer s_id);   //通过id删除学生信息
     int addStudent(Student student);   //添加学生信息
     int updateStudent(Student student); //修改学生信息
     Student findStudentById(Integer s_id);
     List<Student> getAll();
}
