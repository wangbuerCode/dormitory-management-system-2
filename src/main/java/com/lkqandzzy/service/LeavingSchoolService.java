package com.lkqandzzy.service;

import com.lkqandzzy.entity.LeavingSchool;
import com.lkqandzzy.entity.PageInfo;

import java.util.List;

/**
 * @author 邹靓仔and小帅龙
 * @date 2019/12/29-13:35-星期日-DormitoryManager-ssm
 */
public interface LeavingSchoolService {

    //分页查询
     PageInfo<LeavingSchool> findPageInfo(Integer k_classid, String k_dormbuilding, Integer pageIndex, Integer pageSize);

     int deleteLeavingSchool(Integer k_id);   //删除班级信息
     int addLeavingSchool(LeavingSchool leavingSchool);    //添加班级信息
     int updateLeavingSchool(LeavingSchool leavingSchool); //修改班级信息
     LeavingSchool findLeavingSchoolById(Integer k_id);
     List<LeavingSchool> getAll();
}
