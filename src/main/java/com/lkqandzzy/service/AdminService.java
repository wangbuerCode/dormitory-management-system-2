package com.lkqandzzy.service;

import com.lkqandzzy.entity.Admin;
import com.lkqandzzy.entity.PageInfo;

import java.util.List;

/**
 * @author 邹靓仔and小帅龙
 * @date 2019/12/29-13:29-星期日-DormitoryManager-ssm
 */
public interface AdminService {
    // 通过账号和密码查询用户

    Admin findAdmin(Admin admin);

    //找到所有所有数据
     List<Admin> getAll();

    //分页查询
     PageInfo<Admin> findPageInfo(String a_username, String a_describe, Integer a_id, Integer pageIndex, Integer pageSize);

     int addAdmin(Admin admin);    //添加管理员信息
     int deleteAdmin(Integer a_id);   //删除管理员信息
     int updateAdmin(Admin admin); //修改管理员信息
     Admin findAdminById(Integer a_id);
}
