package com.lkqandzzy.service;

import com.lkqandzzy.entity.PageInfo;

import java.util.List;

/**
 * @author 邹靓仔and小帅龙
 * @date 2019/12/29-13:32-星期日-DormitoryManager-ssm
 */
public interface ClassService {

    //分页查询
     PageInfo<Class> findPageInfo(String c_classname, String c_counsellor, Integer c_classid, Integer pageIndex, Integer pageSize);

     int deleteClass(Integer c_id);   //删除班级信息
     int addClass(Class ucalss);    //添加班级信息
     Class findClassById(Integer c_id);
     int updateClass(Class uclass); //修改班级信息
     List<Class> findClassStudent(Class uclass);//查询班级人员信息
     List<Class> getAll();

}
