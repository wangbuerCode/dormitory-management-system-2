package com.lkqandzzy.service;

import com.lkqandzzy.entity.Dlreturn;
import com.lkqandzzy.entity.DormClean;
import com.lkqandzzy.entity.PageInfo;

import java.util.List;

/**
 * @author 邹靓仔and小帅龙
 * @date 2019/12/29-13:34-星期日-DormitoryManager-ssm
 */
public interface DormCleanService {

    //分页查询
     PageInfo<DormClean> findPageInfo(Integer d_id, String d_dormbuilding, Integer pageIndex, Integer pageSize);

     int addDormClean(DormClean dormclean);    //添加宿舍信息
     int deleteDormClean(Integer g_id);   //删除宿舍信息
     int updateDormClean(DormClean dormclean); //修改宿舍信息
     DormClean findDormCleanById(Integer g_id);
     List<DormClean> getAll();

    /**
     * @author 邹靓仔and小帅龙
     * @date 2019/12/29-13:32-星期日-DormitoryManager-ssm
     */
    interface DlreturnService {

        //分页查询
        PageInfo<Dlreturn> findPageInfo(String l_lreturnnanme, Integer l_studentid, Integer l_dormitoryid,
                                        String reason, Integer pageIndex, Integer pageSize);

        int deleteDlreturn(Integer l_id);   //通过id删除晚归学生信息
        int addDlreturn(Dlreturn dlreturn);   //添加晚归学生信息
        int updateDlreturn(Dlreturn dlreturn); //修改晚归学生信息
        Dlreturn findDlreturnId(Integer l_id);
        List<Dlreturn> getAll();
    }
}
