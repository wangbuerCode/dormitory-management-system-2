package com.lkqandzzy.controller;

import com.lkqandzzy.entity.Dlreturn;
import com.lkqandzzy.entity.PageInfo;
import com.lkqandzzy.service.DormCleanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author 邹靓仔and小帅龙
 * @date 2019/12/29-14:09-星期日-DormitoryManager-ssm
 */
@Controller
public class DlreturnController {

    @Autowired
    private DormCleanService.DlreturnService dlreturnService;


    /**
     * 分页查询
     * pageIndex 当前页码
     * pageSize  显示条数
     */
    @RequestMapping(value = "/finDlreturn")
    public String findStudent(String l_lreturnnanme, Integer l_studentid,Integer l_dormitoryid, String reason,
                              Integer pageIndex, Integer pageSize, Model model) {

        PageInfo<Dlreturn> pi = dlreturnService.findPageInfo(l_lreturnnanme,l_studentid,l_dormitoryid,
                reason,pageIndex,pageSize);
        model.addAttribute("pi",pi);
        model.addAttribute("l_lreturnnanme",l_lreturnnanme);
        return "dlreturn_list";
    }

    /**
     * 导出Excel
     */
    @RequestMapping(value = "/exportDlreturnlist", method = RequestMethod.POST)
    @ResponseBody
    public List<Dlreturn> exportStudent(){
        List<Dlreturn> studentList = dlreturnService.getAll();
        return studentList;
    }
    /**
     * 删除学生信息
     */
    @RequestMapping( "/deleteDlreturn")
    @ResponseBody
    public String deleteDlreturn(Integer l_id) {
        int s = dlreturnService.deleteDlreturn(l_id);
        return "dlreturn_list";
    }
    /**
     * 添加学生信息
     */

    @RequestMapping(value = "/addDlreturn" ,method = RequestMethod.POST)
    @ResponseBody
    public String addDlreturn(@RequestBody Dlreturn dlreturn) {
        int s = dlreturnService.addDlreturn(dlreturn);
        return "dlreturn_list";
    }
    /**
     * 修改学生信息
     */
    @RequestMapping(value = "/updateDlreturn" ,method = RequestMethod.POST )
    public String updateDlreturn( Dlreturn dlreturn) {
        int s = dlreturnService.updateDlreturn(dlreturn);
        return "redirect:/finDlreturn";
    }

    @RequestMapping( "/findDlreturnById")
    public String findDormCleanById(Integer l_id, HttpSession session) {

        Dlreturn s= dlreturnService.findDlreturnId(l_id);
        session.setAttribute("s",s);
        return "dlreturn_edit";
    }
}
