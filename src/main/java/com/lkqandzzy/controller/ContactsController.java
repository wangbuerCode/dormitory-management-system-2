package com.lkqandzzy.controller;

import com.lkqandzzy.entity.Contacts;
import com.lkqandzzy.entity.PageInfo;
import com.lkqandzzy.service.ContactsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
public class ContactsController {
    // 依赖注入
    @Autowired
    private ContactsService contactsService;

    /**
     * 分页查询
     * pageIndex 当前页码
     * pageSize  显示条数
     */
    @RequestMapping(value = "/findContacts")
    public String findContacts(String c_subordinate, Integer pageIndex, Integer pageSize, Model model) {

        PageInfo<Contacts> ci = contactsService.findPageInfo(c_subordinate,pageIndex,pageSize);
        model.addAttribute("ci",ci);
        return "contacts_list";
    }

    /**
     * 添加联系人信息
     */
    @RequestMapping(value = "/addContacts" ,method = RequestMethod.POST)
    @ResponseBody
    public String addContacts( @RequestBody Contacts contacts) {
        int c = contactsService.addContacts(contacts);
        return "contacts_list";
    }

    /**
     * 删除联系人信息
     */
    @RequestMapping( "/deleteContacts")
    @ResponseBody
    public String deleteContacts(Integer c_id) {
        int c = contactsService.deleteContacts(c_id);
        return "contacts_list";
    }

    /**
     * 修改联系人信息
     */
    @RequestMapping( "/updateContacts")
    public String updateContacts( Contacts contacts) {
        int c = contactsService.updateContacts(contacts);
        return "redirect:/findContacts";
    }


    @RequestMapping( "/findContactsById")
    public String findContactsById(Integer c_id, HttpSession session) {

        Contacts c= contactsService.findContactsById(c_id);
        session.setAttribute("c",c);
        return "contacts_edit";
    }
}
