package com.lkqandzzy.controller;

import com.lkqandzzy.entity.Client;
import com.lkqandzzy.entity.PageInfo;
import com.lkqandzzy.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
@Controller
public class ClientController {
    // 依赖注入
    @Autowired
    private ClientService clientService;

    /**
     * 分页查询
     * pageIndex 当前页码
     * pageSize  显示条数
     */
    @RequestMapping(value = "/findClient")
    public String findDormitory(String d_name, Integer pageIndex, Integer pageSize, Model model) {

        PageInfo<Client> di = clientService.findPageInfo(d_name,pageIndex,pageSize);
        model.addAttribute("di",di);
        return "client_list";
    }

    /**
     * 添加客户信息
     */
    @RequestMapping(value = "/addClient" ,method = RequestMethod.POST)
    @ResponseBody
    public String addClient( @RequestBody Client client) {
        int d = clientService.addClient(client);
        return "client_list";
    }

    /**
     * 删除客户信息
     */
    @RequestMapping( "/deleteClient")
    @ResponseBody
    public String deleteClient(Integer d_id) {
        int d = clientService.deleteClient(d_id);
        return "client_list";
    }

    /**
     * 修改客户信息
     */
    @RequestMapping( "/updateClient")
    public String updateDormitory( Client client) {
        int d = clientService.updateClient(client);
        return "redirect:/findClient";
    }


    @RequestMapping( "/findClientById")
    public String findClientById(Integer d_id, HttpSession session) {

        Client d= clientService.findClientById(d_id);
        session.setAttribute("d",d);
        return "client_edit";
    }
}
