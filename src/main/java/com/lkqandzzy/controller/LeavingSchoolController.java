package com.lkqandzzy.controller;

import com.lkqandzzy.entity.LeavingSchool;
import com.lkqandzzy.entity.PageInfo;
import com.lkqandzzy.service.LeavingSchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author 邹靓仔and小帅龙
 * @date 2019/12/29-14:16-星期日-DormitoryManager-ssm
 */
@Controller
public class LeavingSchoolController {

    // 依赖注入
    @Autowired
    private LeavingSchoolService leavingSchoolService;

    /**
     * 分页查询
     * pageIndex 当前页码
     * pageSize  显示条数
     */
    @RequestMapping(value = "/findLeavingSchool")
    public String findLeavingSchool(Integer k_classid,String k_dormbuilding,
                                    Integer pageIndex, Integer pageSize, Model model) {

        PageInfo<LeavingSchool> ki = leavingSchoolService.findPageInfo(k_classid,k_dormbuilding,
                pageIndex,pageSize);
        model.addAttribute("ki",ki);
        return "leavingSchool_list";
    }

    /**
     * 导出Excel
     */
    @RequestMapping(value = "/exportleavingSchoollist", method = RequestMethod.POST)
    @ResponseBody
    public List<LeavingSchool> exportLeavingSchool(){
        List<LeavingSchool> leavingSchoolList = leavingSchoolService.getAll();
        return leavingSchoolList;
    }


    /**
     * 添加宿舍信息
     */
    @RequestMapping(value = "/addLeavingSchool" ,method = RequestMethod.POST)
    @ResponseBody
    public String addLeavingSchool( @RequestBody LeavingSchool leavingSchool) {
        int d = leavingSchoolService.addLeavingSchool(leavingSchool);
        return "leavingSchool_list";
    }

    /**
     * 删除宿舍信息
     */
    @RequestMapping( "/deleteLeavingSchool")
    @ResponseBody
    public String deleteLeavingSchool(Integer k_id) {
        int d = leavingSchoolService.deleteLeavingSchool(k_id);
        return "leavingSchool_list";
    }

    /**
     * 修改学生信息
     */
    @RequestMapping( "/updateLeavingSchool")
    public String updateLeavingSchool( LeavingSchool leavingSchool) {
        int ki = leavingSchoolService.updateLeavingSchool(leavingSchool);
        return "redirect:/findLeavingSchool";
    }


    @RequestMapping( "/findLeavingSchoolById")
    public String findLeavingSchoolById(Integer k_id, HttpSession session) {

        LeavingSchool ki= leavingSchoolService.findLeavingSchoolById(k_id);
        session.setAttribute("ki",ki);
        return "leavingSchool_edit";
    }
}
