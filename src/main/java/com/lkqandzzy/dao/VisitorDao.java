package com.lkqandzzy.dao;

import com.lkqandzzy.entity.Visitor;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 邹靓仔and小帅龙
 * @date 2019/12/29-13:26-星期日-DormitoryManager-ssm
 */
public interface VisitorDao {
    /**
     * 进行分页查询
     */

    //获取总条数
    //@Param注解单一属性
     Integer totalCount(@Param("v_name") String v_name, @Param("v_phone") Integer v_phone);
    //获取用户列表
    //@Param注解单一属性
     List<Visitor> getVisitorList(@Param("v_name") String v_name, @Param("v_phone") Integer v_phone, @Param("currentPage") Integer currentPage, @Param("pageSize") Integer pageSize);

     int addVisitor(Visitor visitor);   //添加学生信息
     List<Visitor> getAll();
}
