package com.lkqandzzy.dao;

import com.lkqandzzy.entity.Dlreturn;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 邹靓仔and小帅龙
 * @date 2019/12/29-13:22-星期日-DormitoryManager-ssm
 */
public interface DlreturnDao {

    /**
     * 进行分页查询
     */

    //获取总条数
    //@Param注解单一属性
    Integer totalCount(@Param("l_lreturnnanme") String l_lreturnnanme, @Param("l_studentid") Integer l_studentid,
                       @Param("l_dormitoryid") Integer l_dormitoryid, @Param("reason") String reason);
    //获取用户列表
    //@Param注解单一属性
    List<Dlreturn> getDlreturnList(@Param("l_lreturnnanme") String l_lreturnnanme, @Param("l_studentid") Integer l_studentid, @Param("l_dormitoryid") Integer l_dormitoryid,
                                   @Param("reason") String Reason, @Param("currentPage") Integer currentPage, @Param("pageSize") Integer pageSize);

    int deleteDlreturn(Integer l_id);   //删除晚归学生信息
    int addDlreturn(Dlreturn dlreturn);   //添加晚归学生信息
    int updateDlreturn(Dlreturn dlreturn); //修改晚归学生信息
    Dlreturn findDlreturnId(Integer l_id);
    List<Dlreturn> getAll();
}
