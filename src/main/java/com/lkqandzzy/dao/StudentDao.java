package com.lkqandzzy.dao;

import com.lkqandzzy.entity.Student;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 邹靓仔and小帅龙
 * @date 2019/12/29-13:25-星期日-DormitoryManager-ssm
 */
public interface StudentDao {
    /**
     * 进行分页查询
     */

    //获取总条数
    //@Param注解单一属性
     Integer totalCount(@Param("s_name") String s_name, @Param("s_studentid") Integer s_studentid,
                              @Param("s_classid") Integer s_classid, @Param("s_classname") String s_classname);
    //获取用户列表
    //@Param注解单一属性
     List<Student> getStudentList(@Param("s_name") String s_name, @Param("s_studentid") Integer s_studentid, @Param("s_classid") Integer s_classid,
                                        @Param("s_classname") String s_classname, @Param("currentPage") Integer currentPage, @Param("pageSize") Integer pageSize);

     int deleteStudent(Integer s_id);   //删除学生信息
     int addStudent(Student student);   //添加学生信息
     int updateStudent(Student student); //修改学生信息
     Student findStudentById(Integer s_id);
     List<Student> getAll();
}
