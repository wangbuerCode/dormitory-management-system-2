package com.lkqandzzy.dao;

import com.lkqandzzy.entity.Contacts;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ContactsDao {

    /**
     * 进行分页查询
     */

    //获取总条数
    //@Param注解单一属性
     Integer totalCount(@Param("c_subordinate") String c_subordinate);
    //获取用户列表
    //@Param注解单一属性
     List<Contacts> getDormitoryList(@Param("c_subordinate") String c_subordinate, @Param("currentPage") Integer currentPage, @Param("pageSize") Integer pageSize);

     int addContacts(Contacts contacts);    //添加联系人信息
     int deleteContacts(Integer c_id);   //删除联系人信息
     int updateContacts(Contacts contacts); //修改联系人信息
    Contacts findContactsById(Integer c_id);
     List<Contacts> getAll();
}
