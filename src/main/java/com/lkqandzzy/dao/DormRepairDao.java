package com.lkqandzzy.dao;

import com.lkqandzzy.entity.DormRepair;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 邹靓仔and小帅龙
 * @date 2019/12/29-13:24-星期日-DormitoryManager-ssm
 */
public interface DormRepairDao {
    /**
     * 进行分页查询
     */

    //获取总条数
    //@Param注解单一属性
    Integer totalCount(@Param("d_id") Integer d_id, @Param("d_dormbuilding") String d_dormbuilding);
    //获取用户列表
    //@Param注解单一属性
    List<DormRepair> getDormRepairList(@Param("d_id") Integer d_id, @Param("d_dormbuilding") String d_dormbuilding, @Param("currentPage") Integer currentPage, @Param("pageSize") Integer pageSize);

    int addDormRepair(DormRepair dormrepair);    //添加宿舍信息
    int deleteDormRepair(Integer r_id);   //删除宿舍信息
    int updateDormRepair(DormRepair dormrepair); //修改宿舍信息
    DormRepair findDormRepairById(Integer r_id);
    List<DormRepair> getAll();
}
