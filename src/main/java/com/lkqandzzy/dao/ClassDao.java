package com.lkqandzzy.dao;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 邹靓仔and小帅龙
 * @date 2019/12/29-13:22-星期日-DormitoryManager-ssm
 */
public interface ClassDao {
    /**
     * 进行分页查询
     */

    //获取总条数
    //@Param注解单一属性
    Integer totalCount(@Param("c_classname") String c_classname, @Param("c_classid") Integer c_classid, @Param("c_counsellor") String c_counsellor);
    //获取用户列表
    //@Param注解单一属性
    List<Class> getClassList(@Param("c_classname") String c_classname, @Param("c_classid") Integer c_classid, @Param("c_counsellor") String c_counsellor, @Param("currentPage") Integer currentPage, @Param("pageSize") Integer pageSize);

    int deleteClass(Integer c_id);   //删除班级信息
    int addClass(Class ucalss);    //添加班级信息
    int updateClass(Class uclass); //修改班级信息
    Class findClassById(Integer c_id);
    List<Class> findClassStudent(Class uclass);//查询班级人员信息
    List<Class> getAll();


}
