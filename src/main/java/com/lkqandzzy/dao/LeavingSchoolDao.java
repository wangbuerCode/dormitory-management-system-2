package com.lkqandzzy.dao;

import com.lkqandzzy.entity.LeavingSchool;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 邹靓仔and小帅龙
 * @date 2019/12/29-13:24-星期日-DormitoryManager-ssm
 */
public interface LeavingSchoolDao {

    //获取总条数
    //@Param注解单一属性
    Integer totalCount(@Param("k_classid") Integer k_classid, @Param("k_dormbuilding") String k_dormbuilding);
    //获取用户列表
    //@Param注解单一属性
    List<LeavingSchool> getLeavingSchoolList(@Param("k_classid") Integer k_classid, @Param("k_dormbuilding") String k_dormbuilding, @Param("currentPage") Integer currentPage, @Param("pageSize") Integer pageSize);

    int deleteLeavingSchool(Integer k_id);   //删除晚归信息
    int addLeavingSchool(LeavingSchool leavingSchool);    //添加晚归信息
    int updateLeavingSchool(LeavingSchool leavingSchool); //修改晚归信息
    LeavingSchool findLeavingSchoolById(Integer k_id);
    List<LeavingSchool> getAll();
}
