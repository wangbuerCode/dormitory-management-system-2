package com.lkqandzzy.dao;

import com.lkqandzzy.entity.DormClean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 邹靓仔and小帅龙
 * @date 2019/12/29-13:23-星期日-DormitoryManager-ssm
 */
public interface DormCleanDao {
    /**
     * 进行分页查询
     */
    //获取总条数
    //@Param注解单一属性
     Integer totalCount(@Param("d_id") Integer d_id, @Param("d_dormbuilding") String d_dormbuilding);
    //获取用户列表
    //@Param注解单一属性
     List<DormClean> getDormCleanList(@Param("d_id") Integer d_id, @Param("d_dormbuilding") String d_dormbuilding, @Param("currentPage") Integer currentPage, @Param("pageSize") Integer pageSize);

     int addDormClean(DormClean dormclean);    //添加宿舍卫生信息
     int deleteDormClean(Integer g_id);   //删除宿舍卫生信息
     int updateDormClean(DormClean dormclean); //修改宿舍卫生信息
     DormClean findDormCleanById(Integer g_id);
     List<DormClean> getAll();
}
