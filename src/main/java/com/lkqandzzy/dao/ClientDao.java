package com.lkqandzzy.dao;

import com.lkqandzzy.entity.Client;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ClientDao {

    /**
     * 进行分页查询
     */

    //获取总条数
    //@Param注解单一属性
     Integer totalCount(@Param("d_name") String d_name);
    //获取用户列表
    //@Param注解单一属性
     List<Client> getDormitoryList(@Param("d_name") String d_name,@Param("currentPage") Integer currentPage, @Param("pageSize") Integer pageSize);

     int addClient(Client client);    //添加客户信息
     int deleteClient(Integer d_id);   //删除客户信息
     int updateClient(Client client); //修改客户信息
     Client findClientById(Integer d_id);
     List<Client> getAll();
}
