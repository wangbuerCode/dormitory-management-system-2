package com.lkqandzzy.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * @author 邹靓仔and小帅龙
 * @date 2019/12/29-13:15-星期日-DormitoryManager-ssm
 */
public class LeavingSchool {

    private Integer k_id;
    private Integer k_classid;
    private String k_username;
    private String k_dormbuilding;

    //@JsonFormat注解是一个时间格式化
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date create_time;

    private String k_reason;
    private String k_valuables;

    public Integer getK_id() {
        return k_id;
    }

    public void setK_id(Integer k_id) {
        this.k_id = k_id;
    }

    public Integer getK_classid() {
        return k_classid;
    }

    public void setK_classid(Integer k_classid) {
        this.k_classid = k_classid;
    }

    public String getK_username() {
        return k_username;
    }

    public void setK_username(String k_username) {
        this.k_username = k_username;
    }

    public String getK_dormbuilding() {
        return k_dormbuilding;
    }

    public void setK_dormbuilding(String k_dormbuilding) {
        this.k_dormbuilding = k_dormbuilding;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public String getK_reason() {
        return k_reason;
    }

    public void setK_reason(String k_reason) {
        this.k_reason = k_reason;
    }

    public String getK_valuables() {
        return k_valuables;
    }

    public void setK_valuables(String k_valuables) {
        this.k_valuables = k_valuables;
    }

    @Override
    public String toString() {
        return "LeavingSchool{" +
                "k_id=" + k_id +
                ", k_classid=" + k_classid +
                ", k_username='" + k_username + '\'' +
                ", k_dormbuilding='" + k_dormbuilding + '\'' +
                ", create_time=" + create_time +
                ", k_reason='" + k_reason + '\'' +
                ", k_valuables='" + k_valuables + '\'' +
                '}';
    }
}
