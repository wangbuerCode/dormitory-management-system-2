package com.lkqandzzy.entity;

/**
 * @author 邹靓仔and小帅龙
 * @date 2019/12/29-13:17-星期日-DormitoryManager-ssm
 */
public class Contacts {

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getC_id() {
        return c_id;
    }

    public void setC_id(Integer c_id) {
        this.c_id = c_id;
    }

    public Integer getC_phone() {
        return c_phone;
    }

    public void setC_phone(Integer c_phone) {
        this.c_phone = c_phone;
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    public String getC_sex() {
        return c_sex;
    }

    public void setC_sex(String c_sex) {
        this.c_sex = c_sex;
    }

    public String getC_age() {
        return c_age;
    }

    public void setC_age(String c_age) {
        this.c_age = c_age;
    }

    public String getC_address() {
        return c_address;
    }

    public void setC_address(String c_address) {
        this.c_address = c_address;
    }

    public String getC_subordinate() {
        return c_subordinate;
    }

    public void setC_subordinate(String c_subordinate) {
        this.c_subordinate = c_subordinate;
    }

    private static final long serialVersionUID = 1L;
    private  Integer c_id;
    private  Integer c_phone;
    private  String  c_name;
    private  String  c_sex;
    private  String  c_age;
    private  String  c_address;
    private  String  c_subordinate;


}
