package com.lkqandzzy.entity;
public class Client {

    private static final long serialVersionUID = 1L;
    private  Integer d_id;
    private  Integer d_phone;
    private  String  d_name;
    private  String  d_sex;
    private  String  d_age;
    private  String  d_address;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getD_id() {
        return d_id;
    }

    public void setD_id(Integer d_id) {
        this.d_id = d_id;
    }

    public Integer getD_phone() {
        return d_phone;
    }

    public void setD_phone(Integer d_phone) {
        this.d_phone = d_phone;
    }

    public String getD_name() {
        return d_name;
    }

    public void setD_name(String d_name) {
        this.d_name = d_name;
    }

    public String getD_sex() {
        return d_sex;
    }

    public void setD_sex(String d_sex) {
        this.d_sex = d_sex;
    }

    public String getD_age() {
        return d_age;
    }

    public void setD_age(String d_age) {
        this.d_age = d_age;
    }

    public String getD_address() {
        return d_address;
    }

    public void setD_address(String d_address) {
        this.d_address = d_address;
    }
}
