package com.lkqandzzy.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author 邹靓仔and小帅龙
 * @date 2019/12/29-13:14-星期日-DormitoryManager-ssm
 */
public class Dlreturn {
    private int l_id;
    private String l_lreturnnanme;
    private int l_studentid;
    private int l_dormitoryid;
    //@JsonFormat注解是一个时间格式化
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private String night_time;

    private String reason;

    public int getL_id() {
        return l_id;
    }

    public void setL_id(int l_id) {
        this.l_id = l_id;
    }

    public String getL_lreturnnanme() {
        return l_lreturnnanme;
    }

    public void setL_lreturnnanme(String l_lreturnnanme) {
        this.l_lreturnnanme = l_lreturnnanme;
    }

    public int getL_studentid() {
        return l_studentid;
    }

    public void setL_studentid(int l_studentid) {
        this.l_studentid = l_studentid;
    }

    public int getL_dormitoryid() {
        return l_dormitoryid;
    }

    public void setL_dormitoryid(int l_dormitoryid) {
        this.l_dormitoryid = l_dormitoryid;
    }

    public String getNight_time() {
        return night_time;
    }

    public void setNight_time(String night_time) {
        this.night_time = night_time;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "Dlreturn{" +
                "l_id=" + l_id +
                ", l_lreturnnanme='" + l_lreturnnanme + '\'' +
                ", l_studentid=" + l_studentid +
                ", l_dormitoryid=" + l_dormitoryid +
                ", night_time=" + night_time +
                ", reason='" + reason + '\'' +
                '}';
    }
}
