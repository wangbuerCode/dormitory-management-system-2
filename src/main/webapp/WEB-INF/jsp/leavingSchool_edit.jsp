<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>修改信息</title>
    <link rel="icon" href="images/favicon.ico" sizes="32x32" />
    <link rel="stylesheet" href="css/font.css">
    <link rel="stylesheet" href="css/xadmin.css">
    <link rel="stylesheet" href="css/pg_btn.css">
    <script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
    <script src="lib/layui/layui.js"></script>
    <script type="text/javascript" src="js/xadmin.js"></script>
    <style type="text/css">
        form{
            color: #23262E;
            text-align: center;
        }
        input{
            font-size: 22px;
            font-family:'DFKai-SB';
        }
        xblock{
            background-color: rgba(133,24,171,.31);
            border-radius: 10px;
        }
        body{
            background: url("images/2.png") no-repeat;
            height: auto;
            background-size: cover;
        }
        span{
            font-size: 20px;
            font-family:'DFKai-SB';
        }
        #btn_on{
            width: 190px;
            margin-left: 20px;
        }
    </style>
</head>

<body>

<div class="x-body">
    <form class="layui-form"  id="f_auto" action="${pageContext.request.contextPath}/updateLeavingSchool" method="post" >
        <input type="hidden" value="${sessionScope.ki.k_id}" name="k_id" id="k_id"/>
        <div class="layui-form-item">
            <label for="k_classid" class="layui-form-label">
                <span class="">宿舍编号</span>
            </label>
            <div class="layui-input-inline">
                <input type="text" id="k_classid" name="k_classid"
                       autocomplete="off" value="${sessionScope.ki.k_classid}" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label for="k_username" class="layui-form-label">
                <span class="">名字</span>
            </label>
            <div class="layui-input-inline">
                <input type="text" id="k_username" name="k_username"
                       autocomplete="off" value="${sessionScope.ki.k_username}" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label for="k_dormbuilding" class="layui-form-label">
                <span class="">宿舍楼</span>
            </label>
            <div class="layui-input-inline">
                <input type="text" id="k_dormbuilding" name="k_dormbuilding"
                       autocomplete="off" value="${sessionScope.ki.k_dormbuilding}" class="layui-input">
            </div>
        </div>
        <input type="hidden" value="${sessionScope.ki.create_time}" name="create_time" id="create_time"/>

        <div class="layui-form-item">
            <label for="k_reason" class="layui-form-label">
                <span class="">离校原因</span>
            </label>
            <div class="layui-input-inline">
                <input type="text" id="k_reason" name="k_reason"
                       autocomplete="off" value="${sessionScope.ki.k_reason}" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label for="k_valuables" class="layui-form-label">
                <span class="">贵重物品</span>
            </label>
            <div class="layui-input-inline">
                <input type="text" id="k_valuables" name="k_valuables"
                       autocomplete="off" value="${sessionScope.ki.k_valuables}" class="layui-input">
            </div>
        </div>


        <div class="layui-form-item" id="btn_xg">
            <button  class="layui-btn" id="btn_on"  lay-submit="" lay-filter="updateLeavingSchool">
                修改
            </button>
        </div>
    </form>
</div>

<script>
</script>
</body>
</html>
