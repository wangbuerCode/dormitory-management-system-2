<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>修改信息</title>
    <link rel="icon" href="images/favicon.ico" sizes="32x32" />
    <link rel="stylesheet" href="css/font.css">
    <link rel="stylesheet" href="css/xadmin.css">
    <link rel="stylesheet" href="css/pg_btn.css">
    <script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
    <script src="lib/layui/layui.js"></script>
    <script type="text/javascript" src="js/xadmin.js"></script>
    <style type="text/css">
        form{
            color: #23262E;
            text-align: center;
        }
        input{
            font-size: 22px;
            font-family:'DFKai-SB';
        }
        xblock{
            background-color: rgba(133,24,171,.31);
            border-radius: 10px;
        }
        body{
            background: url("images/2.png") no-repeat;
            height: auto;
            background-size: cover;
        }
        span{
            font-size: 20px;
            font-family:'DFKai-SB';
        }
        #btn_on{
            width: 190px;
            margin-left: 20px;
        }
    </style>
</head>

<body>

<div class="x-body">
    <form class="layui-form"  id="f_auto" action="${pageContext.request.contextPath}/updateDormRepair" method="post" >
        <input type="hidden" value="${sessionScope.d.r_id}" name="r_id" id="r_id"/>
        <div class="layui-form-item">
            <label for="d_id" class="layui-form-label">
                <span class="">宿舍编号</span>
            </label>
            <div class="layui-input-inline">
                <input type="text" id="d_id" name="d_id"
                       autocomplete="off" value="${sessionScope.d.d_id}" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label for="d_dormbuilding" class="layui-form-label">
                <span class="">宿舍楼</span>
            </label>
            <div class="layui-input-inline">
                <input type="text" id="d_dormbuilding" name="d_dormbuilding"
                       autocomplete="off" value="${sessionScope.d.d_dormbuilding}" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label for="r_name" class="layui-form-label">
                <span class="">维修人员</span>
            </label>
            <div class="layui-input-inline">
                <input type="text" id="r_name" name="r_name"
                       autocomplete="off" value="${sessionScope.d.r_name}" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label for="reason" class="layui-form-label">
                <span class="">报修事由</span>
            </label>
            <div class="layui-input-inline">
                <input type="text" id="reason" name="reason"
                       autocomplete="off" value="${sessionScope.d.reason}" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label for="create_time" class="layui-form-label">
                <span class="">报修事由</span>
            </label>
            <div class="layui-input-inline">
                <input type="text" id="create_time" name="create_time"
                       autocomplete="off" value="${sessionScope.d.create_time}" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label for="update_time" class="layui-form-label">
                <span class="">报修事由</span>
            </label>
            <div class="layui-input-inline">
                <input type="text" id="update_time" name="update_time"
                       autocomplete="off" value="${sessionScope.d.update_time}" class="layui-input">
            </div>
        </div>



        <div class="layui-form-item" id="btn_xg">
            <button  class="layui-btn" id="btn_on"  lay-submit="" lay-filter="updateClass">
                修改
            </button>
        </div>
    </form>
</div>

<script>
</script>
</body>
</html>

