<%@ page contentType="text/html;charset=UTF-8" language="java"  import="com.lkqandzzy.entity.Client" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
    <title>后台登录</title>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="icon" href="images/favicon.ico" sizes="32x32" />
    <link rel="stylesheet" href="css/font.css">
    <link rel="stylesheet" href="css/xadmin.css">
    <script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
    <script src="lib/layui/layui.js"></script>
    <script type="text/javascript" src="js/xadmin.js"></script>
    <script src="layui_exts/excel.js"></script>

</head>

<body>
<div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a href="/findClient">客户信息</a>
      </span>
</div>
<div class="x-body">
    <div class="layui-row">
        <form class="layui-form layui-col-md12 x-so" action="${pageContext.request.contextPath}/findClient" >
            <input class="layui-input" placeholder="请输入客户姓名" name="d_name" id="d_name">
            <input class="layui-input" type="hidden" name="pageIndex" value="1">
            <input class="layui-input" type="hidden" name="pageSize" value="3">
            <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>

        </form>
        <button id="addStudnetBtn" class="layui-btn layui-btn-normal">添加 </button>
    </div>

    <%--添加模态框--%>
    <div class="layui-row" id="test" style="display: none;">
        <div class="layui-col-md10">
            <form class="layui-form" id="addEmployeeForm">
                <div class="layui-form-item">
                    <label class="layui-form-label">客户电话：</label>
                    <div class="layui-input-block">
                        <input type="text" name="d_phone" class="layui-input" placeholder="请输入客户电话">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">客户姓名：</label>
                    <div class="layui-input-block">
                        <input type="text" name="d_name" class="layui-input" placeholder="请输入客户姓名">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">客户性别：</label>
                    <div class="layui-input-block">
                        <input type="text" name="d_sex" class="layui-input" placeholder="请输入客户性别">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">客户年龄：</label>
                    <div class="layui-input-block">
                        <input type="text" name="d_age" class="layui-input" placeholder="请输入客户年龄">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">客户地址：</label>
                    <div class="layui-input-block">
                        <input type="text" name="d_address" class="layui-input" placeholder="请输入客户地址">
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button type="button" class="layui-btn layui-btn-normal" lay-submit lay-filter="formDemo">提交</button>
                        <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <%--表格数据--%>
    <table class="layui-table">
        <thead>
        <tr>
            <th>ID</th>
            <th>客户电话</th>
            <th>客户姓名</th>
            <th>客户性别</th>
            <th>客户年龄</th>
            <th>客户地址</th>
            <th>操作</th>
        </thead>
        <tbody>
<c:forEach items="${di.list}" var="di">
        <tr>
            <td>${di.d_id}</td>
            <td>${di.d_phone}</td>
            <td>${di.d_name}</td>
            <td>${di.d_sex}</td>
            <td>${di.d_age}</td>
            <td>${di.d_address}</td>
            <td>
                <button class="layui-btn layui-btn-normal " >
                <a title="编辑"    id= "updateEdit"    href="${pageContext.request.contextPath}/findClientById?d_id=${di.d_id}" style="color: #cee4f1">
                    修改
                </a>
                </button>
                <button class="layui-btn layui-btn-normal">
                <a title="删除" onclick="member_del(this,'${di.d_id}')" href="javascript:;">
                    删除
                </a>
                </button>
            </td>
        </tr>
</c:forEach>
        </tbody>
    </table>

<div class="" >
    <input type="hidden" id="totalPageCount" value="${di.pageTotalCount}"/>
    <c:import url="pageBtn.jsp">
        <c:param name="totalCount" value="${di.totalCount}"/>
        <c:param name="currentPageNo" value="${di.pageIndex}"/>
        <c:param name="totalPageCount" value="${di.pageTotalCount}"/>
    </c:import>
</div>
<script>

    layui.config({
        base: 'layui_exts/',
    }).extend({
        excel: 'excel',
    });

    layui.use(['jquery', 'excel','form','layer','laydate'], function(){
        var form = layui.form,
            $ = layui.jquery,
            laydate = layui.laydate;
        var excel = parent.layui.excel;

        //执行一个laydate实例
        laydate.render({
            elem: '#start' //指定元素
        });

        form.on('submit(toolbarDemo)', function(){

            $.ajax({
                url: '${pageContext.request.contextPath}/exportdormitorylist',
                type: 'post',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    console.log(data);

                    // 1. 如果需要调整顺序，请执行梳理函数
                    var dt = excel.filterExportData(data, [
                        'd_id'
                        ,'d_phone'
                        ,'d_name'
                        ,'d_sex'
                        ,'d_age'
                        ,'d_address'
                    ]);

                    // 2. 数组头部新增表头
                    dt.unshift({d_id: 'ID', d_phone: '客户电话', d_name: '客户姓名', d_sex: '客户性别', d_age: '客户年龄', d_address: '客户地址'});

                    // 意思是：A列40px，B列60px(默认)，C列120px，D、E、F等均未定义
                    var colConf = excel.makeColConfig({
                        'C': 90,
                        'F': 80
                    }, 60);

                    var timestart = Date.now();
                    // 3. 执行导出函数，系统会弹出弹框
                    excel.exportExcel({
                        sheet1: dt
                    }, '客户数据.xlsx', 'xlsx', {
                        extend: {
                            '!cols': colConf
                        }
                    });
                    var timeend = Date.now();

                    var spent = (timeend - timestart) / 1000;
                    layer.alert('导出耗时 '+spent+' s');
                    //setTimeout(function () {window.location.href='/findAdmin';},2000);
                },

                error: function () {
                    //console.log(data);
                    setTimeout(function () {window.location.href='/findClient';},2000);
                }
            });
        });

        /*添加弹出框*/
        $("#addStudnetBtn").click(function () {
            layer.open({
                type:1,
                title:"添加客户",
                skin:"myclass",
                area:["50%"],
                anim:2,
                content:$("#test").html()
            });
            $("#addEmployeeForm")[0].reset();
            form.on('submit(formDemo)', function(data) {
                // layer.msg('aaa',{icon:1,time:3000});
                var param=data.field;
                // console.log(JSON.stringify(param));
                $.ajax({
                    url: '${pageContext.request.contextPath}/addClient',
                    type: "post",
                    data:JSON.stringify(param),
                    contentType: "application/json; charset=utf-8",
                    success:function(){
                            layer.msg('添加成功', {icon: 1, time: 3000});
                            setTimeout(function () {window.location.href='${pageContext.request.contextPath}/findClient';},2000);

                    },
                    error:function(){
                        layer.msg('添加失败',{icon:0,time:3000});
                        setTimeout(function () {window.location.href='${pageContext.request.contextPath}/findClient';},2000);
                    }
                });
                // return false;
            });
        });

    });


    /*删除*/
    function member_del(obj,d_id){
        layer.confirm('确认要删除吗？',function(index){
            //发异步删除数据
            $.get("${pageContext.request.contextPath}/deleteClient",{"d_id":d_id},function (data) {
                if(data =true){
                    layer.msg('删除成功!',{icon:1,time:2000});
                    setTimeout(function () {window.location.href='${pageContext.request.contextPath}/findClient';},2000);

                }else {
                    layer.msg('删除失败!',{icon:1,time:2000});
                    setTimeout(function () {window.location.href='${pageContext.request.contextPath}/findClient';},2000);
                }
            });
        });
    }

</script>


</body>


</html>
