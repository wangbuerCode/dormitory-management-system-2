<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>后台登录</title>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <%--<meta http-equiv="Cache-Control" content="no-siteapp" />--%>

    <link rel="icon" href="images/favicon.ico" sizes="32x32" />
    <link rel="stylesheet" href="css/font.css">
    <link rel="stylesheet" href="css/xadmin.css">
    <script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
    <script src="lib/layui/layui.js"></script>
    <script type="text/javascript" src="js/xadmin.js"></script>
    <script src="layui_exts/excel.js"></script>

    <style>
        .o_div{
            background: url("images/2.png") no-repeat center fixed;
            height: auto;
            background-size: cover;
        }
        .o_span{
            display: block;
            text-align: center;
            font-size: 20px;
            letter-spacing:8px;
            height: 70px;
            font-family: 'DFKai-SB';
            color: #130303;
            background-color: rgba(195,47,178,.23);
            border-radius: 20px;
        }
        .layui-card{
            border-radius: 20px;
            height: 500px;
            width: 1000px;
            background-color: rgba(33,36,37,.29);
            font-family: 'DFKai-SB';
        }
        .logo a{
            font-family: 'DFKai-SB';
        }
        #btn7 a{
            font-size: 20px;
            font-family: 'DFKai-SB';
        }
        .layui-tab-title{
            background-color: rgba(210,163,163,.6);
        }
        .copyright{
            font-size: 20px;
            font-family: 'DFKai-SB';
        }
        .layui-nav-img{
            width:45px;
            height:45px;
            margin-right:10px;
            border-radius:50%
        }
        .left-nav{
            background-color: rgba(212,195,195,1);
        }
        .page-content{
            background-color: rgba(212,195,195,1);
        }
    </style>
</head>
<body>
<!-- 顶部开始 -->
<div class="container">
    <div class="logo"><a href="">宿舍管理系统</a></div>
    <div class="left_open">
        <i title="展开左侧栏" class="iconfont">&#xe699;</i>
    </div>

    <ul class="layui-nav right" lay-filter="" id="btn7">
        <li class="layui-nav-item">
            <a href="javascript:;"><img src="images/laonianren.jpg" class="layui-nav-img">${sessionScope.ad.a_username}</a>
            <dl class="layui-nav-child"> <!-- 二级菜单 -->
                <dd><a onclick="x_admin_show('切换帐号','loginOut')">切换帐号</a></dd>
                <dd><a href="${pageContext.request.contextPath}/loginOut">退出</a></dd>
            </dl>
        </li>
        <li class="layui-nav-item to-index"><a href="${pageContext.request.contextPath}/loginOut">前台首页</a></li>
    </ul>

</div>
<!-- 顶部结束 -->
<!-- 中部开始 -->
<!-- 左侧菜单开始 -->
<div class="left-nav">
    <div id="side-nav">
        <ul id="nav">

<c:if test="${sessionScope.ad.a_power==1}">
            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe6b8;</i>
                    <cite>学生管理</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu" >
                    <li  style="background-color: rgba(210,163,163,.6);">
                        <a _href="${pageContext.request.contextPath}/findStudent">
                            <%--点击在右侧出现动态的Tab--%>
                            <i class="iconfont">&#xe6a7;</i>
                            <cite >学生信息</cite>

                        </a>
                    </li >

                </ul>
            </li>
</c:if>
<%--            <li>--%>
<%--                <a href="javascript:;">--%>
<%--                    <i class="iconfont">&#xe723;</i>--%>
<%--                    <cite>班级管理</cite>--%>
<%--                    <i class="iconfont nav_right">&#xe697;</i>--%>
<%--                </a>--%>
<%--                <ul class="sub-menu">--%>
<%--                    <li>--%>
<%--                        <a _href="${pageContext.request.contextPath}/findClass">--%>
<%--                            &lt;%&ndash;点击在右侧出现动态的Tab&ndash;%&gt;--%>
<%--                            <i class="iconfont">&#xe6a7;</i>--%>
<%--                            <cite>班级列表</cite>--%>
<%--                        </a>--%>
<%--                    </li >--%>
<%--                </ul>--%>
<%--            </li>--%>


<%--            <li>--%>
<%--                <a href="javascript:;">--%>
<%--                    <i class="iconfont">&#59095;</i>--%>
<%--                    <cite>客户管理</cite>--%>
<%--                    <i class="iconfont nav_right">&#xe697;</i>--%>
<%--                </a>--%>
<%--                <ul class="sub-menu">--%>
<%--                    <li>--%>
<%--                        <a _href="${pageContext.request.contextPath}/findClient">--%>
<%--                            &lt;%&ndash;点击在右侧出现动态的Tab&ndash;%&gt;--%>
<%--                            <i class="iconfont">&#xe6a7;</i>--%>
<%--                            <cite>客户列表</cite>--%>
<%--                        </a>--%>
<%--                    </li >--%>
<%--                </ul>--%>

<%--                <ul class="sub-menu">--%>
<%--                    <li>--%>
<%--                        <a _href="${pageContext.request.contextPath}/findContacts">--%>
<%--                            &lt;%&ndash;点击在右侧出现动态的Tab&ndash;%&gt;--%>
<%--                            <i class="iconfont">&#xe6a7;</i>--%>
<%--                            <cite>客户联系人</cite>--%>
<%--                        </a>--%>
<%--                    </li >--%>
<%--                </ul>--%>

<%--            </li>--%>

            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#59042;</i>
                    <cite>卫生管理</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>

                <ul class="sub-menu">
                    <li>
                        <a _href="${pageContext.request.contextPath}/findDormClean">
                            <%--点击在右侧出现动态的Tab--%>
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>宿舍卫生</cite>
                        </a>
                    </li >
                </ul>

                <ul class="sub-menu">
                    <li>
                        <a _href="${pageContext.request.contextPath}/findStudentClean">
                            <%--点击在右侧出现动态的Tab--%>
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>学生卫生</cite>
                        </a>
                    </li >
                </ul>

            </li>

            <li>
                <a href="javascript:;">
                    <i class="layui-icon">&#xe613;</i>
                    <cite>访客管理</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="${pageContext.request.contextPath}/findVisitor">
                            <%--点击在右侧出现动态的Tab--%>
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>访客列表</cite>
                        </a>
                    </li >
                </ul>
            </li>
            <c:if test="${sessionScope.ad.a_power==1}">
            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe726;</i>
                    <cite>管理员管理</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="${pageContext.request.contextPath}/findAdmin">
                            <%--点击在右侧出现动态的Tab--%>
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>管理员列表</cite>
                        </a>
                    </li >


                </ul>
            </li>

            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#59042;</i>
                    <cite>离校管理</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="${pageContext.request.contextPath}/findLeavingSchool">
                            <%--点击在右侧出现动态的Tab--%>
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>离校名单</cite>
                        </a>
                    </li >
                </ul>
            </li>
            </c:if>
            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe723;</i>
                    <cite>晚归管理</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="${pageContext.request.contextPath}/finDlreturn">
                            <%--点击在右侧出现动态的Tab--%>
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>晚归名单</cite>
                        </a>
                    </li >
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe723;</i>
                    <cite>维修管理</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="${pageContext.request.contextPath}/findDormRepair">
                            <%--点击在右侧出现动态的Tab--%>
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>维修信息</cite>
                        </a>
                    </li >
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- <div class="x-slide_left"></div> -->
<!-- 左侧菜单结束 -->
<!-- 右侧主体开始 -->
<div class="page-content">
    <div class="layui-tab tab" lay-filter="xbs_tab" lay-allowclose="false">
        <ul class="layui-tab-title" style="background-color: rgba(212,195,195,1);">
            <li class="home"><i class="layui-icon">&#xe68e;</i>我的桌面</li>
        </ul>
        <div class="layui-tab-content" >
            <div class="layui-tab-item layui-show o_div" >
                <span class="o_span"><br>${sessionScope.ad.a_username}管理员，欢迎来到宿舍管理系统！<br></span>
                <div class="layui-col-md6" style="padding: 130px;left: 200px;background-color: rgba(255, 255, 255, .0);">
                <div class="layui-card">
                        <div class="layui-card-header" style="font-family: 'DFKai-SB';font-size: 25px" align="center">
                            个人信息
                        </div>
                        <br><br>
                        <div class="layui-card-body" style="font-family: 'DFKai-SB';font-size: 25px" align="center">
                            <%--<div class="layui-form-label" style="text-align: left">你好</div>--%>
                           用户名：${sessionScope.ad.a_username}
                        </div>
                        <div class="layui-card-body" style="font-family: 'DFKai-SB';font-size: 25px" align="center">
                            姓名：${sessionScope.ad.a_name}
                        </div>
                        <div class="layui-card-body" style="font-family: 'DFKai-SB';font-size: 25px" align="center">
                            电话：${sessionScope.ad.a_phone}
                        </div>
                        <div class="layui-card-body" style="font-family: 'DFKai-SB';font-size: 25px" align="center">
                            级别描述：${sessionScope.ad.a_describe}
                        </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="page-content-bg"></div>
<!-- 右侧主体结束 -->
<!-- 中部结束 -->
<!-- 底部开始 -->
<div class="footer">
</div>
<!-- 底部结束 -->

<script>
    layui.config({
        base: 'layui_exts/',
    }).extend({
        excel: 'excel',
    });
</script>
</body>
</html>
