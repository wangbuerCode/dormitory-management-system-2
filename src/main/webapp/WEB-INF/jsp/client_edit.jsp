<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>修改信息</title>
    <link rel="icon" href="images/favicon.ico" sizes="32x32" />
    <link rel="stylesheet" href="css/font.css">
    <link rel="stylesheet" href="css/xadmin.css">
    <link rel="stylesheet" href="css/pg_btn.css">
    <script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
    <script src="lib/layui/layui.js"></script>
    <script type="text/javascript" src="js/xadmin.js"></script>
    <style type="text/css">
        form{
            color: #23262E;
            text-align: center;
        }
        input{
            font-size: 22px;
            font-family:'DFKai-SB';
        }
        xblock{
            background-color: rgba(133,24,171,.31);
            border-radius: 10px;
        }
        span{
            font-size: 20px;
            font-family:'DFKai-SB';
        }
        #btn_on{
            width: 190px;
            margin-left: 20px;
        }
    </style>
</head>

<body>

<div class="x-body">
    <form class="layui-form"  id="f_auto" action="${pageContext.request.contextPath}/updateClient" method="post" >
        <input type="hidden" value="${sessionScope.d.d_id}" name="d_id" id="d_id"/>
        <div class="layui-form-item">
            <label for="d_phone" class="layui-form-label">
                <span class="">客户电话</span>
            </label>
            <div class="layui-input-inline">
                <input type="text" id="d_phone" name="d_phone"
                       autocomplete="off" value="${sessionScope.d.d_phone}" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label for="d_name" class="layui-form-label">
                <span class="">客户姓名</span>
            </label>
            <div class="layui-input-inline">
                <input type="text" id="d_name" name="d_name"
                       autocomplete="off" value="${sessionScope.d.d_name}" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label for="d_sex" class="layui-form-label">
                <span class="">客户性别</span>
            </label>
            <div class="layui-input-inline">
                <input type="text" id="d_sex" name="d_sex"
                       autocomplete="off" value="${sessionScope.d.d_sex}" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label for="d_age" class="layui-form-label">
                <span class="">客户年龄</span>
            </label>
            <div class="layui-input-inline">
                <input type="text" id="d_age" name="d_age"
                       autocomplete="off" value="${sessionScope.d.d_age}" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label for="c_address" class="layui-form-label">
                <span class="">客户地址</span>
            </label>
            <div class="layui-input-inline">
                <input type="text" id="c_address" name="c_address"
                       autocomplete="off" value="${sessionScope.c.c_address}" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label for="c_address" class="layui-form-label">
                <span class="">客户地址</span>
            </label>
            <div class="layui-input-inline">
                <input type="text" id="c_address" name="c_address"
                       autocomplete="off" value="${sessionScope.c.c_address}" class="layui-input">
            </div>
        </div>


        <div class="layui-form-item" id="btn_xg">
            <button  class="layui-btn" id="btn_on"  lay-submit="" lay-filter="updateClass">
                修改
            </button>
        </div>
    </form>
</div>

<script>

</script>
</body>
</html>
