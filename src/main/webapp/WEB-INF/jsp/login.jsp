<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="css/usersLogin.css">
    <link rel="icon" href="images/favicon.ico" sizes="32x32" />
    <script src="js/jquery-1.3.2.min.js"></script>
    <script src="js/login.js"></script>
    <style>
        input.iputs{
            background-color: rgba(255, 255, 255, .7);
            border-radius: 10px;
            font-size: 25px;
        }
        div .panel{
            background-color: rgba(255, 255, 255, .4);
            border-radius: 20px;
        }
        #btn3{
            background-color: darksalmon;
            width: 150px;
            height: 40px;
            border-radius: 10px;
            font-size: 28px;
            font-family: 'DFKai-SB';
            color: #4E5465;
        }
        .top{
            font-family: 'DFKai-SB';
        }
        .body{
            background: url("images/2.png")no-repeat center fixed;
            height: auto;
            background-size: cover;
            width: 100%;
            height: 700px;
        }
        .header{
            background: url("images/bti.jpg")no-repeat;
            width: 100%;
            height: 200px;
        }
        .middle>form>.s1{
            height: 40px;
            width: 40px;
            position: absolute;
            left: 35px;
            top: 28px;
            background-image:url("images/12346.jpg");
            background-repeat: no-repeat;
        }
        .middle>form>.s2{
            height: 40px;
            width: 40px;
            position: absolute;
            left: 35px;
            top: 115px;
            background-image:url("images/12345.jpg");
            background-repeat: no-repeat;
        }
    </style>
    <title>前台首页</title>
</head>
<body>

<div class="header">

</div>

<div class="body">
    <div class="panel " >
        <div class="top">
            <p style="color: #4E5465">账户登陆</p>
        </div>

        <div class="middle" >
            <form action="${pageContext.request.contextPath}/login" method="post">

                <span class="erro">${msg}</span>
                <span class="s1"></span>
                <span class="s2"></span>
                <input id="userName" type="text" name="a_username" placeholder="请输入用户名" value="" class="iputs" style="height: 50px;">
                <input id="password" type="password" name="a_password" placeholder="请输入密码" value="" class="iputs" style="height: 50px;">
                <br>
                <br>
                <div align="center">
                    <input type="submit" value="登陆" align="center" id="btn3">
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
