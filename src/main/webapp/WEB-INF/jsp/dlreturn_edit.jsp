<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>修改信息</title>
    <link rel="icon" href="images/favicon.ico" sizes="32x32" />
    <link rel="stylesheet" href="css/font.css">
    <link rel="stylesheet" href="css/xadmin.css">
    <link rel="stylesheet" href="css/pg_btn.css">
    <script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
    <script src="lib/layui/layui.js"></script>
    <script type="text/javascript" src="js/xadmin.js"></script>
    <style type="text/css">
        form{
            color: #23262E;
            text-align: center;
        }
        input{
            font-size: 22px;
            font-family:'DFKai-SB';
        }
        xblock{
            background-color: rgba(133,24,171,.31);
            border-radius: 10px;
        }
        body{
            background: url("images/2.png") no-repeat;
            height: auto;
            background-size: cover;
        }
        span{
            font-size: 20px;
            font-family:'DFKai-SB';
        }
        #btn_on{
            width: 190px;
            margin-left: 20px;
        }
    </style>
</head>

<body>

<div class="x-body">
    <form class="layui-form" action="${pageContext.request.contextPath}/updateDlreturn" method="post"  id="f_auto" accept-charset="UTF-8">
        <input type="hidden" value="${sessionScope.s.l_id}" name="l_id" id="l_id"/>

        <div class="layui-form-item">
            <label for="l_lreturnnanme" class="layui-form-label">
                <span class="f_sp">姓名</span>
            </label>
            <div class="layui-input-inline">
                <input type="text" id="l_lreturnnanme" name="l_lreturnnanme"
                       autocomplete="off" value="${sessionScope.s.l_lreturnnanme}" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label for="l_studentid" class="layui-form-label">
                <span class="f_sp">学号</span>
            </label>
            <div class="layui-input-inline">
                <input type="text" id="l_studentid" name="l_studentid"
                       autocomplete="off" value="${sessionScope.s.l_studentid}" class="layui-input">
            </div>
        </div>


        <div class="layui-form-item">
            <label for="l_dormitoryid" class="layui-form-label">
                <span class="f_sp">宿舍号</span>
            </label>
            <div class="layui-input-inline">
                <input type="text" id="l_dormitoryid" name="l_dormitoryid"
                       autocomplete="off" value="${sessionScope.s.l_dormitoryid}" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label for="night_time" class="layui-form-label">
                <span class="f_sp">时间</span>
            </label>
            <div class="layui-input-inline">
                <input type="text" id="night_time" name="night_time"
                       autocomplete="off" value="${sessionScope.s.night_time}" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label for="reason" class="layui-form-label">
                <span class="f_sp">原因</span>
            </label>
            <div class="layui-input-inline">
                <input type="text" id="reason" name="reason"
                       autocomplete="off" value="${sessionScope.s.reason}" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item" id="btn_xg">
            <button  class="layui-btn"  id="btn_on" lay-filter="updateForm" lay-submit="">
                修改
            </button>
        </div>
    </form>
</div>


</body>
</html>
