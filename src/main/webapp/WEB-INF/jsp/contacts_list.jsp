<%@ page contentType="text/html;charset=UTF-8" language="java"  import="com.lkqandzzy.entity.Client" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
    <title>后台登录</title>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="icon" href="images/favicon.ico" sizes="32x32" />
    <link rel="stylesheet" href="css/font.css">
    <link rel="stylesheet" href="css/xadmin.css">
    <script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
    <script src="lib/layui/layui.js"></script>
    <script type="text/javascript" src="js/xadmin.js"></script>
    <script src="layui_exts/excel.js"></script>

</head>

<body>
<div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a href="/findContacts">客户联系人</a>
      </span>
</div>
<div class="x-body">
    <div class="layui-row">
        <form class="layui-form layui-col-md12 x-so" action="${pageContext.request.contextPath}/findContacts" >
            <input class="layui-input" placeholder="请输入所属客户" name="c_subordinate" id="c_subordinate">
            <input class="layui-input" type="hidden" name="pageIndex" value="1">
            <input class="layui-input" type="hidden" name="pageSize" value="3">
            <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
        </form>
        <button id="addStudnetBtn" class="layui-btn layui-btn-normal">添加 </button>
    </div>

    <%--添加模态框--%>
    <div class="layui-row" id="test" style="display: none;">
        <div class="layui-col-md10">
            <form class="layui-form" id="addEmployeeForm">
                <div class="layui-form-item">
                    <label class="layui-form-label">联系人电话：</label>
                    <div class="layui-input-block">
                        <input type="text" name="c_phone" class="layui-input" placeholder="请输入客户电话">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">联系人姓名：</label>
                    <div class="layui-input-block">
                        <input type="text" name="c_name" class="layui-input" placeholder="请输入客户姓名">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">联系人性别：</label>
                    <div class="layui-input-block">
                        <input type="text" name="c_sex" class="layui-input" placeholder="请输入客户性别">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">联系人年龄：</label>
                    <div class="layui-input-block">
                        <input type="text" name="c_age" class="layui-input" placeholder="请输入客户年龄">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">联系人地址：</label>
                    <div class="layui-input-block">
                        <input type="text" name="c_address" class="layui-input" placeholder="请输入客户地址">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">联系人客户：</label>
                    <div class="layui-input-block">
                        <input type="text" name="c_subordinate" class="layui-input" placeholder="请输入客户地址">
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button type="button" class="layui-btn layui-btn-normal" lay-submit lay-filter="formDemo">提交</button>
                        <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <%--表格数据--%>
    <table class="layui-table">
        <thead>
        <tr>
            <th>ID</th>
            <th>联系人电话</th>
            <th>联系人姓名</th>
            <th>联系人性别</th>
            <th>联系人年龄</th>
            <th>联系人地址</th>
            <th>所属客户</th>
            <th>操作</th>
        </thead>
        <tbody>
<c:forEach items="${ci.list}" var="ci">
        <tr>
            <td>${ci.c_id}</td>
            <td>${ci.c_phone}</td>
            <td>${ci.c_name}</td>
            <td>${ci.c_sex}</td>
            <td>${ci.c_age}</td>
            <td>${ci.c_address}</td>
            <td>${ci.c_subordinate}</td>
            <td>
                <button class="layui-btn layui-btn-normal " >
                <a title="编辑"    id= "updateEdit"    href="${pageContext.request.contextPath}/findContactsById?c_id=${ci.c_id}" style="color: #cee4f1">
                    修改
                </a>
                </button>
                <button class="layui-btn layui-btn-normal">
                <a title="删除" onclick="member_del(this,'${ci.c_id}')" href="javascript:;" style="color: #cee4f1">
                    删除
                </a>
                </button>
            </td>
        </tr>
</c:forEach>
        </tbody>
    </table>

<div class="" >
    <input type="hidden" id="totalPageCount" value="${ci.pageTotalCount}"/>
    <c:import url="pageBtn.jsp">
        <c:param name="totalCount" value="${ci.totalCount}"/>
        <c:param name="currentPageNo" value="${ci.pageIndex}"/>
        <c:param name="totalPageCount" value="${di.pageTotalCount}"/>
    </c:import>
</div>
<script>

    layui.config({
        base: 'layui_exts/',
    }).extend({
        excel: 'excel',
    });

    layui.use(['jquery', 'excel','form','layer','laydate'], function(){
        var form = layui.form,
            $ = layui.jquery,
            laydate = layui.laydate;
        var excel = parent.layui.excel;

        //执行一个laydate实例
        laydate.render({
            elem: '#start' //指定元素
        });

        form.on('submit(toolbarDemo)', function(){

            $.ajax({
                url: '${pageContext.request.contextPath}/exportdormitorylist',
                type: 'post',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    console.log(data);

                    // 1. 如果需要调整顺序，请执行梳理函数
                    var dt = excel.filterExportData(data, [
                        'c_id'
                        ,'c_phone'
                        ,'c_name'
                        ,'c_sex'
                        ,'c_age'
                        ,'c_address'
                        ,'c_subordinate'
                    ]);

                    // 2. 数组头部新增表头
                    dt.unshift({c_id: 'ID', c_phone: '联系人电话', c_name: '联系人姓名', c_sex: '联系人性别', c_age: '联系人年龄', c_address: '联系人地址', c_subordinate: '所属客户'});

                    // 意思是：A列40px，B列60px(默认)，C列120px，D、E、F等均未定义
                    var colConf = excel.makeColConfig({
                        'C': 90,
                        'F': 80
                    }, 60);

                    var timestart = Date.now();
                    // 3. 执行导出函数，系统会弹出弹框
                    excel.exportExcel({
                        sheet1: dt
                    }, '联系人数据.xlsx', 'xlsx', {
                        extend: {
                            '!cols': colConf
                        }
                    });
                    var timeend = Date.now();

                    var spent = (timeend - timestart) / 1000;
                    layer.alert('导出耗时 '+spent+' s');
                    //setTimeout(function () {window.location.href='/findAdmin';},2000);
                },

                error: function () {
                    //console.log(data);
                    setTimeout(function () {window.location.href='/findContacts';},2000);
                }
            });
        });

        /*添加弹出框*/
        $("#addStudnetBtn").click(function () {
            layer.open({
                type:1,
                title:"添加联系人",
                skin:"myclass",
                area:["50%"],
                anim:2,
                content:$("#test").html()
            });
            $("#addEmployeeForm")[0].reset();
            form.on('submit(formDemo)', function(data) {
                // layer.msg('aaa',{icon:1,time:3000});
                var param=data.field;
                // console.log(JSON.stringify(param));
                $.ajax({
                    url: '${pageContext.request.contextPath}/addContacts',
                    type: "post",
                    data:JSON.stringify(param),
                    contentType: "application/json; charset=utf-8",
                    success:function(){
                            layer.msg('添加成功', {icon: 1, time: 3000});
                            setTimeout(function () {window.location.href='${pageContext.request.contextPath}/findContacts';},2000);

                    },
                    error:function(){
                        layer.msg('添加失败',{icon:0,time:3000});
                        setTimeout(function () {window.location.href='${pageContext.request.contextPath}/findContacts';},2000);
                    }
                });
                // return false;
            });
        });

    });


    /*删除*/
    function member_del(obj,c_id){
        layer.confirm('确认要删除吗？',function(index){
            //发异步删除数据
            $.get("${pageContext.request.contextPath}/deleteContacts",{"c_id":c_id},function (data) {
                if(data =true){
                    layer.msg('删除成功!',{icon:1,time:2000});
                    setTimeout(function () {window.location.href='${pageContext.request.contextPath}/findContacts';},2000);

                }else {
                    layer.msg('删除失败!',{icon:1,time:2000});
                    setTimeout(function () {window.location.href='${pageContext.request.contextPath}/findContacts';},2000);
                }
            });
        });
    }

</script>


</body>


</html>
